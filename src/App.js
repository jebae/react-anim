import React from 'react';
import Header from './components/Header';
import PostContainer from './containers/PostContainer';

class App extends React.Component{
  render(){
    return (
      <div>
        <Header/>
        <PostContainer/>
      </div>
    )
  }
}

export default App;

//
