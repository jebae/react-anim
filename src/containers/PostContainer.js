import React from 'react';
import PostWrapper from '../components/PostWrapper';
import Navigate from '../components/Navigate';
import Post from '../components/Post';
import Warning from '../components/Warning';
import * as services from '../services/post';

class PostContainer extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      postId: 1,
      fetching: false,
      post: {
        title: null,
        body: null
      },
      comments: [],
      warningVisibility: false,
      has_error: false
    }
  }

  // fetch post json data
  fetchPostInfo = async (postId) => {
    this.setState({
      fetching: true
    });

    try{
      const info = await Promise.all([
        services.getPost(postId),
        services.getComments(postId)
      ]);

      const { title, body } = info[0].data;
      const comments = info[1].data;

      this.setState({
        postId,
        post: {
          title,
          body
        },
        comments,
        fetching: false
      })
    } catch (err){
      this.setState({
        fetching: false
      })
      console.log('error', err);
      this.showWarning();
    }
  }

  // handle navigate click
  handleNavigateClick = (type) => {
    const postId = this.state.postId;

    switch (type){
      case 'NEXT':
        this.fetchPostInfo(postId + 1);
        break;
      case 'PREV':
        this.fetchPostInfo(postId - 1);
        break;
    }
  }

  // show warning
  showWarning = () => {
    this.setState({
      warningVisibility: true
    });
  
    setTimeout(
      () => {
        this.setState({ warningVisibility: false })
      }, 1500
    )
  }

  componentDidMount(){
    this.fetchPostInfo(1);
  }

  render(){
    const {
      postId, post, comments, fetching, warningVisibility
    } = this.state;

    return (
      <PostWrapper>
        <Navigate
          postId={ postId }
          disabled={ fetching }
          onClick={ this.handleNavigateClick }
          />
        <Post
          postId={ postId }
          title={ post.title }
          body={ post.body }
          comments={ comments }
          />
        <Warning message="Error show" visible={ warningVisibility }/>
      </PostWrapper>
    )
  }
}
export default PostContainer;