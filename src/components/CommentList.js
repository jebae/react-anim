import React from 'react';
import '../stylesheets/components/CommentList.scss';
import Comment from './Comment';

const CommentList = ({ comments }) => {
  return (
    <ul className="CommentList">
      {comments.map((v, i) => {
        return (
          <Comment key={ i } comment={ v }/>
        )
      })}
    </ul>
  )
}

export default CommentList;

//
