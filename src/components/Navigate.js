import React from 'react';
import '../stylesheets/components/Navigate.scss';

const Navigate = ({ onClick, postId, disabled }) => {
  return (
    <div
      className="Navigate">
      <button
        className="Navigate-btn-left"
        onClick={ () => onClick('PREV') }
        disabled={ disabled }>
        Previous
      </button>
      <div
        className="Navigate-page-num">
        { postId }
      </div>
      <button
        className="Navigate-btn-right"
        onClick={ () => onClick('NEXT') }
        disabled={ disabled }>
        Next
      </button>
    </div>
  );
}

export default Navigate;
//
