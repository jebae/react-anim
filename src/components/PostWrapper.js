import React from 'react';
import '../stylesheets/components/PostWrapper.scss';

const PostWrapper = ({children}) => {
  return (
    <div
      className="PostWrapper">
      { children }
    </div>
  )
}

export default PostWrapper;
// react is awesome
