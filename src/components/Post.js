import React from 'react';
import CommentList from './CommentList';
import '../stylesheets/components/Post.scss';

class Post extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      postInfo: {
        title: null,
        body: null,
        comments : []
      },
      animate: false,
      direction: 'left'
    }
  }

  componentWillReceiveProps(nextProps){
    const { title, body, comments } = nextProps;
    if (this.props.postId != nextProps.postId){
      const direction = (this.props.postId < nextProps.postId) ? 'left' : 'right';

      this.setState({
        animate: true,
        direction
      })

      setTimeout(
        () => {
          this.setState({
            postInfo: {
              title, body, comments
            },
            animate: false
          })
        }, 500
      )
      return;
    }

    // first post case
    this.setState({
      postInfo: {
        title, body, comments
      }
    })
  }
  render(){
    const { title, body, comments } = this.state.postInfo;
    const { animate, direction } = this.state;

    const animation = animate
      ? (direction=='left' ? 'zoomOutLeft' : 'zoomOutRight')
      : (direction=='left' ? 'zoomInRight' : 'zoomInLeft')
    console.log('animation', animation);
    if (title == null) return null;

    return (
      <div className={`Post animated ${ animation }`}>
        <h1>{ title }</h1>
        <p>{ body }</p>
        <CommentList comments={ comments }/>
      </div>
    )
  }
}

export default Post;