var webpack = require('webpack');

module.exports = {
  entry: [
    './src/index.js',
    'webpack-dev-server/client?http://0.0.0.0:3000',
    'webpack/hot/only-dev-server'
  ],
  output: {
    path: '/',
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: './public',
    port: 3000
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ],
  module:{
    loaders: [
      {
        test: /.js$/,
        loader: 'babel-loader',
        exclude: '/node_modules/',
        query: {
          cacheDirectory: true,
          presets: ['es2015', 'react'],
          plugins: ['react-hot-loader/babel', 'transform-class-properties']
        }
      },
      {
        test: /.scss$/,
        exclude: '/node_modules/',
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            options: {

            }
          }
        ]
      }
    ]
  },
}
